package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//header")
    private WebElement header;

    @FindBy(xpath = "//footer")
    private WebElement footer;

    @FindBy(xpath = "//span[@class='AckDUvD -rhP1cz gBrrjX4 _2nHArcS']")
    private WebElement cartIcon;

    @FindBy(xpath = "//button[@data-testid='myAccountIcon']")
    private WebElement myAccountButton;

    @FindBy(xpath = "//a[@data-testid='savedItemsIcon']")
    private WebElement wishListButton;

    @FindBy(xpath = "//input[@data-testid='search-input']")
    private WebElement searchField;

    @FindBy(xpath = "//button[@data-testid='search-button-inline']")
    private WebElement searchButton;



    public HomePage(WebDriver driver) {
        super(driver);
    }

    public boolean isHeaderVisible() {
        return header.isDisplayed();
    }

    public boolean isFooterVisible() {
        return footer.isDisplayed();
    }

    public boolean isCartIconVisible() {
        return cartIcon.isDisplayed();
    }

    public boolean isWishListButtonVisible() {
        return wishListButton.isDisplayed();
    }

    public boolean isMyAccountButtonVisible() {
        return myAccountButton.isDisplayed();
    }

    public boolean isSearchFieldVisible() {
        return searchField.isDisplayed();
    }

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText, Keys.ENTER);
    }

    public void clickSearchButton(){
        searchButton.click();
    }

}
