package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ProductPage extends BasePage{

    @FindBy(xpath = "//button[@data-test-id='add-button']")
    private WebElement addToCartButton;

    @FindBy(xpath = "//div[@data-testid='minibag-dropdown']")
    private WebElement addToCartPopup;

    @FindBy(xpath="//span[@class='_1z5n7CN']")
    private WebElement countElementsInTheCart;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public void clickAddToCartButton(){
        addToCartButton.click();
    }

    public WebElement getAddToCartButton(){return addToCartButton;}

    public boolean isAddToCartPopupVisible() {
        return addToCartPopup.isDisplayed();
    }

    public String getCountElementsInTheCart() {
        return countElementsInTheCart.getText();
    }
    public WebElement getAddToCartPopup() {
        return addToCartPopup;
    }

    public boolean isAddToCartButtonVisible () {return addToCartButton.isDisplayed(); }


}
