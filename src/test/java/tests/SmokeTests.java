package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class SmokeTests extends BaseTest {

    private static final long DEFAULT_WAITING_TIME = 90;

    @Test
    public void checkMainComponentsOnHomePage() {
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        assertTrue(getHomePage().isHeaderVisible());
        assertTrue(getHomePage().isFooterVisible());
        assertTrue(getHomePage().isSearchFieldVisible());
        assertTrue(getHomePage().isCartIconVisible());
        assertTrue(getHomePage().isMyAccountButtonVisible());
        assertTrue(getHomePage().isWishListButtonVisible());
    }

    @Test
    public void checkAddToCart() {
        getHomePage().waitForPageLoadComplete(DEFAULT_WAITING_TIME);
        getHomePage().waitForAjaxToComplete(DEFAULT_WAITING_TIME);
        getHomePage().isFooterVisible();
        getHomePage().enterTextToSearchField("113028650");
        getProductPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME,getProductPage().getAddToCartButton());
        getProductPage().clickAddToCartButton();
        getProductPage().waitVisibilityOfElement(DEFAULT_WAITING_TIME, getProductPage().getAddToCartPopup());
        assertTrue(getProductPage().isAddToCartPopupVisible());
        assertEquals(getProductPage().getCountElementsInTheCart(), "1");
    }
}


